FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAR_FILE
COPY build/libs/piveraproject-1.0.0.jar app.jar
RUN apk add --update nano
RUN apk add --no-cache tzdata
ENV TZ America/Chicago
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN mkdir configs
COPY configs/permissions.txt configs/permissions.txt
COPY configs/devices.txt configs/devices.txt
COPY configs/vera_details.txt configs/vera_details.txt
ENTRYPOINT ["java","-jar","/app.jar"]