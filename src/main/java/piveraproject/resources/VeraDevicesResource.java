package piveraproject.resources;

import javax.servlet.http.HttpServletRequest;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import piveraproject.dto.FreeformJSON;
import piveraproject.services.VeraDevicesService;
import piveraproject.utilities.Utilities;

@RestController
public class VeraDevicesResource {

  // todo: make this injectable so it can be mocked for code coverage.
  VeraDevicesService veraDevicesService;

  @EventListener(ApplicationReadyEvent.class)
  public void startupActivities() {
    Utilities.logInfo("PiVeraProject server is up!");
    veraDevicesService = new VeraDevicesService();
  }

  /**
   * Get the data for all the devices available on the Vera. This might exclude the "current value"
   * of the device.
   *
   * @param userID
   * @param password
   * @param request
   * @return
   */
  @GetMapping(value = "/devices", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity getAllDevicesForUser(
      @RequestHeader(defaultValue = "") String userID,
      @RequestHeader(defaultValue = "") String password,
      HttpServletRequest request) {
    return veraDevicesService.getAllDevicesForUser(userID, password, request);
  }

  /**
   * Get the value of a single device on the Vera. This will include the "current value" of the
   * device.
   *
   * @param name
   * @param userID
   * @param password
   * @param request
   * @return
   */
  @GetMapping(value = "/device", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity getOneDevicesForUser(
      @RequestParam(defaultValue = "") String name,
      @RequestHeader(defaultValue = "") String userID,
      @RequestHeader(defaultValue = "") String password,
      HttpServletRequest request) {
    return veraDevicesService.getOneDevicesForUser(name, userID, password, request);
  }

  /**
   * Get the value of a single device on the Vera. This will include the "current value" of the
   * device.
   *
   * @param request
   * @return
   */
  @GetMapping(value = "/test", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity test(HttpServletRequest request) {
    return new ResponseEntity("", HttpStatus.NOT_FOUND);
  }

  /**
   * Update the value of a single device on the Vera.
   *
   * @param userID
   * @param password
   * @param name
   * @param input
   * @param request
   * @return
   */
  @PostMapping(value = "/device", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity incomingUpdateRequest_WithNewValue(
      @RequestHeader(defaultValue = "") String userID,
      @RequestHeader(defaultValue = "") String password,
      @RequestParam(defaultValue = "") String name,
      @RequestParam(defaultValue = "") String input,
      HttpServletRequest request) {
    return veraDevicesService.updateDevice_Standard(userID, password, name, input, request);
  }

  /**
   * This is the voice channel, intended to be accessible by 3rd party integrations, such as Google
   * Home/Nest
   *
   * @param body
   * @param request
   * @return
   */
  @PostMapping(value = "/voice", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity incomingUpdateRequest_VoiceChannel(
      @RequestBody FreeformJSON body, HttpServletRequest request) {
    return veraDevicesService.updateDevice_VoiceChannel(body, request);
  }

  /**
   * This is for the twilio channel, intended to be accessible by only Twilio. Validation occurs to
   * in the service layer to validate that the incoming call actually originated from Twilio.
   *
   * @param request
   * @return
   */
  @PostMapping(value = "/twilio")
  public ResponseEntity incomingUpdateRequest_TwilioChannel(HttpServletRequest request) {
    return veraDevicesService.updateDevice_TwilioChannel(request);
  }
}
