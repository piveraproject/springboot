package piveraproject.device_components;

import com.google.gson.annotations.Expose;

public class Name {

  @Expose private String key;
  @Expose private String humanReadable;

  public String getKey() {
    return key;
  }

  public Name setKey(String key) {
    this.key = key;
    return this;
  }

  public String getHumanReadable() {
    return humanReadable;
  }

  public Name setHumanReadable(String humanReadable) {
    this.humanReadable = humanReadable;
    return this;
  }
}
