package piveraproject.device_components;

import com.google.gson.annotations.Expose;

public class SelectableState {
  @Expose private String trigger;

  @Expose(serialize = false) // don't allow this as an outgoing value.
  private int outboundControllerValue;

  @Expose(serialize = false) // don't allow this as an outgoing value.
  private int address;

  @Expose private String buttonText;

  @Expose(deserialize = false) // ignore all incoming config data for this value.
  private String currentValue;

  public void setCurrentValue(String currentValue) {
    this.currentValue = currentValue;
  }

  public String getTrigger() {
    return trigger;
  }

  public int getAddress() {
    return address;
  }

  public int getOutboundControllerValue() {
    return outboundControllerValue;
  }

  public SelectableState setOutboundControllerValue(int outboundControllerValue) {
    this.outboundControllerValue = outboundControllerValue;
    return this;
  }

  public SelectableState setButtonText(String buttonText) {
    this.buttonText = buttonText;
    return this;
  }

  public SelectableState setTrigger(String trigger) {
    this.trigger = trigger;
    return this;
  }

  public SelectableState setAddress(int address) {
    this.address = address;
    return this;
  }
}
