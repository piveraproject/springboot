package piveraproject.services;

import static piveraproject.twilio.TwilioUtils.extractPostParams;
import static piveraproject.twilio.TwilioUtils.getRequestUrlAndQueryString;
import static piveraproject.utilities.Constants.SECONDS_DELAY_BETWEEN_PROCESSING_SUBSEQUENT_VOICE_REQUESTS;
import static piveraproject.utilities.WordManipulator.splitByWordANDandAlsoShareNewValue;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.twilio.security.RequestValidator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import piveraproject.adapters.GenericItemJSONAdapter;
import piveraproject.device_components.SelectableState;
import piveraproject.device_models.GenericItem;
import piveraproject.device_models.IntegerItem;
import piveraproject.device_models.OnOffItem;
import piveraproject.device_models.SceneOnOffReadDeviceStateItem;
import piveraproject.dto.FreeformJSON;
import piveraproject.dto.FreeformPayloadFiltered;
import piveraproject.dto.TwilioConfig;
import piveraproject.dto.User;
import piveraproject.dto.UserPermissions;
import piveraproject.dto.VeraDevices;
import piveraproject.dto.VeraPlatformState;
import piveraproject.exceptions.DeviceNotFoundException;
import piveraproject.exceptions.NewValueNotAllowedException;
import piveraproject.exceptions.NewValueNotParsableException;
import piveraproject.exceptions.StateNotFoundException;
import piveraproject.twilio.TwilioUtils;
import piveraproject.utilities.Utilities;

public class VeraDevicesService {

  VeraDevices veraDevices;
  UserPermissions userPermissions;
  TwilioConfig twilioConfig;
  VeraConnectionService veraConnectionService;

  public VeraDevicesService() {
    veraDevices = new VeraDevices().setDevices(Utilities.readDevicesFromFile());
    userPermissions = new UserPermissions().setPermissionList(Utilities.readPermissionsFromFile());
    veraConnectionService = new VeraConnectionService().init();
    twilioConfig =
        new TwilioConfig().setAuthToken(Utilities.readAuthTokenFromTwilioConfigFromFile());
  }

  public ResponseEntity getAllDevicesForUser(
      String userID, String password, HttpServletRequest request) {
    User matchedPerson = getMatchedPersonByID(userID);

    if (matchedPerson == null) {
      return generateNullPersonResponse(request.getRemoteAddr(), userID, password);
    }

    if (!matchedPerson.isPasswordCorrect(password)) {
      return generatePersonNotFoundResponse(request.getRemoteAddr(), userID, password);
    }

    Utilities.logInfo("Valid GET attempt. ip: " + request.getRemoteAddr() + " userID: " + userID);

    Map<String, String> actualVeraDeviceValues =
        getAllDeviceActualStatusesFromVeraAllAtOnce(veraConnectionService);

    // begin by querying the Vera platform and learning the status of every device.
    Iterator<Map.Entry<String, GenericItem>> itr = veraDevices.getDevices().entrySet().iterator();
    ArrayList<GenericItem> result = new ArrayList<>();
    while (itr.hasNext()) {
      Map.Entry<String, GenericItem> entry = itr.next();
      if (Utilities.doesPersonHavePermission(
          matchedPerson, entry.getValue().getSecurityCategory())) {
        result.add(setDeviceFromActualVeraDeviceStatuses(entry.getValue(), actualVeraDeviceValues));
      }
    }
    return new ResponseEntity(new Gson().toJson(result), HttpStatus.OK);
  }

  private Map<String, String> getAllDeviceActualStatusesFromVeraAllAtOnce(
      VeraConnectionService veraConnectionService) {
    String url = veraConnectionService.VERA_HTTP_GET_ALL_DEVICE_STATUS;

    Map<String, String> result = new HashMap<>();
    JsonElement parsedResponse =
        new JsonParser().parse(new RestTemplate().getForObject(url, String.class));
    if (parsedResponse != null) {
      JsonArray deviceArray = parsedResponse.getAsJsonObject().getAsJsonArray("devices");
      for (JsonElement device : deviceArray) {
        String id = device.getAsJsonObject().get("id").toString();

        JsonArray stateArray = device.getAsJsonObject().get("states").getAsJsonArray();
        VeraPlatformState[] states = new Gson().fromJson(stateArray, VeraPlatformState[].class);

        try {
          VeraPlatformState statusState =
              (VeraPlatformState.findStateByVariableName(states, "Status"));
          if ("0".equalsIgnoreCase(statusState.value)) {
            // the device is off
            result.put(id, statusState.value);
            continue;
          }
          // the device is on.
          result.put(id, "1");

          // check to see if it's a dimmer or fan by attempting to get the dimmer value, if present.
          try {
            result.put(
                id, (VeraPlatformState.findStateByVariableName(states, "LoadLevelStatus")).value);
            continue;
          } catch (StateNotFoundException e) {
            // not a dimmer or a fan. oh well, keep going.
          }
        } catch (StateNotFoundException e) {
          // this device didn't have a 'status' state. ignore it and continue.
        }
      }
    }
    return result;
  }

  private GenericItem setDeviceFromActualVeraDeviceStatuses(
      GenericItem item, Map<String, String> veraStatuses) {
    // order matters here, based on inheritance.
    if (item instanceof SceneOnOffReadDeviceStateItem) {
      String deviceCurrentValueString =
          veraStatuses.get(
              String.valueOf(((SceneOnOffReadDeviceStateItem) item).getActualDeviceAddress()));

      // if the device was deleted on the Vera, the String value returned might be null. If so,
      // ignore it.
      if (!StringUtils.isEmpty(deviceCurrentValueString)) {
        for (SelectableState state : ((SceneOnOffReadDeviceStateItem) item).getEligibleStates()) {
          int deviceCurrentValue = Integer.parseInt(deviceCurrentValueString);
          state.setCurrentValue(
              Utilities.computeButtonValueBasedOnStateAndActualValue(state, deviceCurrentValue));
        }
      }
    } else if (item instanceof IntegerItem) {
      ((IntegerItem) item)
          .setCurrentValue(veraStatuses.get(String.valueOf(((IntegerItem) item).getAddress())));
    } else if (item instanceof OnOffItem) {
      for (int index = 0; index < ((OnOffItem) item).getEligibleStates().size(); index++) {
        for (SelectableState state : ((OnOffItem) item).getEligibleStates()) {
          String deviceCurrentValue = veraStatuses.get(String.valueOf(state.getAddress()));
          if (!String.valueOf(state.getOutboundControllerValue())
              .equalsIgnoreCase(deviceCurrentValue)) {
            state.setCurrentValue("0");
          } else {
            state.setCurrentValue("1");
          }
        }
      }
    }
    return item;
  }

  public ResponseEntity getOneDevicesForUser(
      String name, String userID, String password, HttpServletRequest request) {

    User matchedPerson = getMatchedPersonByID(userID);

    if (matchedPerson == null) {
      return generateNullPersonResponse(request.getRemoteAddr(), userID, password);
    }

    if (!matchedPerson.isPasswordCorrect(password)) {
      return generatePersonNotFoundResponse(request.getRemoteAddr(), userID, password);
    }

    Utilities.logInfo(
        "Valid GET attempt. ip: "
            + request.getRemoteAddr()
            + " userID: "
            + userID
            + " deviceName: "
            + name);
    if (!StringUtils.isEmpty(name)) {
      Iterator<Map.Entry<String, GenericItem>> itr = veraDevices.getDevices().entrySet().iterator();
      while (itr.hasNext()) {
        Map.Entry<String, GenericItem> entry = itr.next();
        if (entry.getValue().getName().getKey().equalsIgnoreCase(name)
            && (Utilities.doesPersonHavePermission(
                matchedPerson, entry.getValue().getSecurityCategory()))) {
          return new ResponseEntity(
              new Gson().toJson(entry.getValue().getAndStoreCurrentValue(veraConnectionService)),
              HttpStatus.OK);
        }
      }
    }
    return new ResponseEntity("Device Not Found", HttpStatus.NOT_FOUND);
  }

  public ResponseEntity updateDevice_VoiceChannel(FreeformJSON body, HttpServletRequest request) {
    return updateDevice_Freeform(body, request);
  }

  public ResponseEntity updateDevice_TwilioChannel(HttpServletRequest request) {
    boolean isValidRequest =
        new RequestValidator(twilioConfig.getAuthToken())
            .validate(
                getRequestUrlAndQueryString(request),
                extractPostParams(request),
                TwilioUtils.getTwilioSignatureFromHeader(request));

    if (isValidRequest) {
      Utilities.logInfo("Twilio invocation successful!");
      User twilioUser = getMatchedPersonByName("twilio");
      FreeformJSON freeformJSON =
          new FreeformJSON()
              .setUserID(twilioUser.getUserID())
              .setPassword(twilioUser.getPassword())
              .setMessage("turn on Realtor Mode");
      updateDevice_Freeform(freeformJSON, request);
      return new ResponseEntity(
          "Welcome! The front door should unlock in a moment!\n\nBefore you leave, please pull the door closed behind you and press your palm against the keypad screen to lock the deadbolt. \n\nThank you! If you have any issues, please txt the homeowner at 706-267-8490.",
          HttpStatus.OK);
    }
    Utilities.logInfo("Error: Twilio invocation unsuccessful.");
    return new ResponseEntity("", HttpStatus.OK);
  }

  private ResponseEntity updateDevice_Freeform(FreeformJSON body, HttpServletRequest request) {
    String userID = body.getUserID();
    String password = body.getPassword();
    String message = body.getMessage();

    if (StringUtils.isEmpty(userID)
        || StringUtils.isEmpty(password)
        || StringUtils.isEmpty(message)) {
      Utilities.logInfo(
          "Bad voice channel attempt; user not recognized or bad password. ip: "
              + request.getRemoteAddr()
              + " userID: "
              + userID
              + " with password: "
              + password
              + " with payload: "
              + message);
      return new ResponseEntity("Access denied.", HttpStatus.UNAUTHORIZED);
    }

    User matchedPerson = getMatchedPersonByID(userID);

    if (matchedPerson == null || !matchedPerson.isPasswordCorrect(password)) {
      Utilities.logInfo(
          "Bad voice channel attempt; user not recognized or bad password. ip: "
              + request.getRemoteAddr()
              + " userID: "
              + userID
              + " with password: "
              + password
              + " with payload: "
              + message);
      return new ResponseEntity("Access denied.", HttpStatus.UNAUTHORIZED);
    }

    Utilities.logInfo(
        "incoming name... ip: "
            + request.getRemoteAddr()
            + " userID: "
            + userID
            + " password: "
            + password
            + " payload: "
            + message);

    ArrayList<FreeformPayloadFiltered> splitResult = null;
    try {
      splitResult = splitByWordANDandAlsoShareNewValue(message);
    } catch (NewValueNotParsableException e) {
      Utilities.logInfo("Voice Channel new value not understood: " + e.getBadValue());
    }

    ArrayList<String> responses = new ArrayList<>();

    for (FreeformPayloadFiltered nameAndPayload : splitResult) {
      Utilities.logInfo(
          "                  name: "
              + nameAndPayload.name
              + " VoicePayload: "
              + nameAndPayload.payload);

      responses.add(
          String.valueOf(
              processUpdateRequest(
                      "[voice channel] " + request.getRemoteAddr(),
                      userID,
                      password,
                      nameAndPayload.name,
                      nameAndPayload.payload,
                      false)
                  .getStatusCodeValue()));

      try {
        TimeUnit.SECONDS.sleep(SECONDS_DELAY_BETWEEN_PROCESSING_SUBSEQUENT_VOICE_REQUESTS);
      } catch (InterruptedException e) {
        Utilities.logJavaError("Thread sleep error: " + e.getCause() + " " + e.getMessage());
      }
    }
    return ResponseEntity.ok(responses);
  }

  public ResponseEntity updateDevice_Standard(
      String userID, String password, String name, String input, HttpServletRequest request) {
    Utilities.logInfo(
        "incoming name... ip: "
            + request.getRemoteAddr()
            + " userID: "
            + userID
            + " name: "
            + name
            + " input: "
            + input);

    return processUpdateRequest(request.getRemoteAddr(), userID, password, name, input, true);
  }

  private ResponseEntity generateNullPersonResponse(
      String address, String userID, String password) {
    Utilities.logInfo(
        "Bad GET attempt. ip: " + address + " userID: " + userID + " with password: " + password);
    return new ResponseEntity("Access denied.", HttpStatus.UNAUTHORIZED);
  }

  private ResponseEntity generatePersonNotFoundResponse(
      String address, String userID, String password) {
    Utilities.logInfo(
        "Bad GET attempt. ip: " + address + " userID: " + userID + " with password: " + password);
    return new ResponseEntity("Request denied.", HttpStatus.FORBIDDEN);
  }

  private User getMatchedPersonByID(final String userID) {
    return userPermissions.getPermissionList().stream()
        .filter(e -> e.getUserID().equalsIgnoreCase(userID))
        .findFirst()
        .orElse(null);
  }

  private User getMatchedPersonByName(final String name) {
    return userPermissions.getPermissionList().stream()
        .filter(e -> e.getName().equalsIgnoreCase(name))
        .findFirst()
        .orElse(null);
  }

  private ResponseEntity processUpdateRequest(
      String address,
      String userID,
      String password,
      String lookupValue,
      String newValue,
      boolean reportBackWithNewDeviceValue) {

    try {
      User matchedPerson = getMatchedPersonByID(userID);

      if (matchedPerson == null) {
        return generateNullPersonResponse(address, userID, password);
      }

      GenericItem targetDevice = veraDevices.getDeviceFromName(lookupValue);

      if (!Utilities.doesPersonHavePermission(matchedPerson, targetDevice.getSecurityCategory())) {
        Utilities.logInfo("Bad attempt from UserID: " + userID);
        return new ResponseEntity("Permission denied.", HttpStatus.FORBIDDEN);
      }

      if (targetDevice.isSecure()
          && (StringUtils.isEmpty(password) || !matchedPerson.isPasswordCorrect(password))) {
        Utilities.logInfo("Bad attempt from UserID: " + userID);
        return new ResponseEntity("Request denied.", HttpStatus.FORBIDDEN);
      }

      int delayBeforeStatusCall = targetDevice.postToVera(newValue, veraConnectionService);

      GsonBuilder gsonBuilder = new GsonBuilder();
      gsonBuilder.registerTypeAdapter(GenericItem.class, new GenericItemJSONAdapter());
      gsonBuilder.excludeFieldsWithoutExposeAnnotation();
      Gson gson = gsonBuilder.create();
      if (reportBackWithNewDeviceValue) {
        try {
          TimeUnit.SECONDS.sleep(delayBeforeStatusCall);
        } catch (InterruptedException e) {
          Utilities.logJavaError("Thread sleep error: " + e.getCause() + " " + e.getMessage());
        }
        return new ResponseEntity(
            gson.toJson(targetDevice.getAndStoreCurrentValue(veraConnectionService)),
            HttpStatus.OK);
      }
      return new ResponseEntity(gson.toJson(targetDevice), HttpStatus.OK);
    } catch (DeviceNotFoundException e) {
      Utilities.logInfo("Device not found: " + lookupValue);
      Utilities.logPossiblePhoneticsFailure(
          "Device not found: " + lookupValue); // for fine-tuning phonetics
      return new ResponseEntity("Request denied.", HttpStatus.BAD_REQUEST);
    } catch (NewValueNotParsableException e) {
      Utilities.logInfo("New value not understood: " + newValue);
      return new ResponseEntity("Request denied.", HttpStatus.BAD_REQUEST);
    } catch (NewValueNotAllowedException e) {
      Utilities.logInfo("New value not allowed: " + newValue);
      return new ResponseEntity("Request denied.", HttpStatus.BAD_REQUEST);
    }
  }
}
