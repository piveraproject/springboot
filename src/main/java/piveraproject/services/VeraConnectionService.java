package piveraproject.services;

import com.google.gson.Gson;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.springframework.util.StringUtils;
import piveraproject.dto.VeraConfig;
import piveraproject.utilities.Utilities;

public class VeraConnectionService {

  public static VeraConnectionService instance;
  // RUN Commands
  public final String VERA_SCENE_RUN = "data_request?id=action";
  public final String VERA_RUN_SCENE_COMMAND = "HomeAutomationGateway1&action=RunScene&SceneNum=";
  public final String VERA_DEVICE_UPDATE = "data_request?id=lu_action&output_format=xml&DeviceNum=";
  public final String VERA_GET_ALL_DEVICE_COMMAND = "data_request?id=status";

  // Device Identifier
  public final String VERA_DEVICE_GETTER = "data_request?id=variableget&DeviceNum=";

  // Service IDs
  public final String VERA_RUN_SCENE_SERVICE_ID = "&serviceId=urn:micasaverde-com:serviceId:";
  public final String VERA_DEVICE_UPDATE_SERVICE_ID = "&serviceId=urn:upnp-org:serviceId:";
  public final String VERA_LOCK_SERVICE_ID = "&serviceId=urn:micasaverde-com:serviceId:";

  // GET Commands
  public final String VERA_ON_OFF_GET_COMMAND = "SwitchPower1&Variable=Status";
  public final String VERA_LOCK_GET_COMMAND = "DoorLock1&Variable=Status";
  public final String VERA_INT_ON_OFF_GET_COMMAND = "Dimming1&Variable=LoadLevelStatus";

  // SET Commands
  public final String VERA_ON_OFF_SET_COMMAND = "SwitchPower1&action=SetTarget&newTargetValue=";
  public final String VERA_INT_ON_OFF_SET_COMMAND =
      "Dimming1&action=SetLoadLevelTarget&newLoadlevelTarget=";

  // Headers and Address, built upon initialization.
  public String VERA_HTTP_DEVICE_UPDATE_HEADER;
  public String VERA_HTTP_SCENE_RUN_UPDATE_HEADER;
  public String VERA_HTTP_GETTER_HEADER;
  public String VERA_HTTP_GET_ALL_DEVICE_STATUS;

  public VeraConnectionService init() {
    String veraIP = "";
    String veraPort = "";

    String json = "";
    try {
      json = new String(Files.readAllBytes(Paths.get("configs/vera_details.txt")));
    } catch (IOException e) {
      Utilities.logJavaError(
          "Exception found while reading Vera Details File: "
              + e.getCause()
              + " "
              + e.getMessage());
    }
    VeraConfig config = null;
    try {
      config = new Gson().fromJson(json, VeraConfig.class);
      if (config == null
          || StringUtils.isEmpty(config.getIp())
          || StringUtils.isEmpty(config.getPort())) {
        Utilities.logJavaError(
            "Exception found while parsing Vera Details File: config missing critical Vera connection details");
        System.exit(0);
      }
    } catch (Exception e) {
      Utilities.logJavaError(
          "Exception found while parsing Vera Details File: "
              + e.getCause()
              + " "
              + e.getMessage());
      System.exit(0);
    }

    // Construct Headers and Address
    String veraAddress = "http://" + config.getIp() + ":" + config.getPort() + "/";
    VERA_HTTP_DEVICE_UPDATE_HEADER = veraAddress + VERA_DEVICE_UPDATE;
    VERA_HTTP_SCENE_RUN_UPDATE_HEADER = veraAddress + VERA_SCENE_RUN;
    VERA_HTTP_GETTER_HEADER = veraAddress + VERA_DEVICE_GETTER;
    VERA_HTTP_GET_ALL_DEVICE_STATUS = veraAddress + VERA_GET_ALL_DEVICE_COMMAND;

    return this;
  }
}
