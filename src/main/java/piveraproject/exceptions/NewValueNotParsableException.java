package piveraproject.exceptions;

public class NewValueNotParsableException extends Exception {
  private String badValue;

  public NewValueNotParsableException(String badValue) {
    this.badValue = badValue;
  }

  public String getBadValue() {
    return badValue;
  }
}
