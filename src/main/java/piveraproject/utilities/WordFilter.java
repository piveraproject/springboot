package piveraproject.utilities;

public class WordFilter {

  public static String filterVoicePayloadPhonetics(String input) {
    input = input.toLowerCase();
    input = input.replace("turn ", "");
    input = input.replace("halfway", "to 50");
    input = input.replace("lights", "light");
    input = input.replace("to high", "to 100");
    input = input.replace("to medium", "to 50");
    input = input.replace("to low", "to 25");
    input = input.replace("on high", "to 100");
    input = input.replace("on medium", "to 50");
    input = input.replace("on low", "to 25");
    input = input.replace("the", "");
    input = input.replace("    ", "   ");
    input = input.replace("   ", "  ");
    input = input.replace("  ", " ");
    return input;
  }

  public static String handlePossibleSplitsUsingAND(String input) {
    input = input.toLowerCase();
    input = input.replace(" of ", " ");
    input = input.replace(" in the ", " and the ");
    input = input.replace(" as well as ", " and ");
    input = input.replace("all the kitchen lights", "the kitchen lights");
    return input;
  }
}
