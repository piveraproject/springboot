package piveraproject.utilities;

import static piveraproject.utilities.Utilities.buildStringFromSubArray;
import static piveraproject.utilities.Utilities.findAnInteger;
import static piveraproject.utilities.Utilities.isNumeric;
import static piveraproject.utilities.Utilities.makeNumberReasonable;
import static piveraproject.utilities.WordFilter.filterVoicePayloadPhonetics;
import static piveraproject.utilities.WordFilter.handlePossibleSplitsUsingAND;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.springframework.util.StringUtils;
import piveraproject.dto.FreeformPayloadFiltered;
import piveraproject.exceptions.NewValueNotParsableException;

public class WordManipulator {
  public static ArrayList<FreeformPayloadFiltered> splitByWordANDandAlsoShareNewValue(
      String inputRaw) throws NewValueNotParsableException {
    inputRaw = handlePossibleSplitsUsingAND(inputRaw);
    String[] inputsArray = inputRaw.split(" and ");
    ArrayList<FreeformPayloadFiltered> outputs = new ArrayList();
    for (String input : inputsArray) {
      outputs.add(convertVoiceInputToDevicesAndPayloads(filterVoicePayloadPhonetics(input)));
    }
    return applyFirstFoundPayloadToAllBlankPayloads(outputs);
  }

  public static ArrayList<FreeformPayloadFiltered> applyFirstFoundPayloadToAllBlankPayloads(
      ArrayList<FreeformPayloadFiltered> outputs) {
    /**
     * Attempt to make sure all items have a payload: 1. see if we have at least one valid payload.
     * If so, 2. see if we have at least one blank payload. If so, 3. apply the valid payload to all
     * items that have a blank payload.
     */
    outputs.stream()
        .filter(output -> !StringUtils.isEmpty(output.payload))
        .findFirst()
        .ifPresent(
            // We have at least one item with a valid payload.
            // Now check to see if we have any items with blank payloads.
            itemWithPayload -> {
              outputs.stream()
                  .filter(output -> StringUtils.isEmpty(output.payload))
                  .forEach(
                      itemWithoutPayload -> {
                        itemWithoutPayload.payload = itemWithPayload.payload;
                      });
            });
    return outputs;
  }

  public static FreeformPayloadFiltered convertVoiceInputToDevicesAndPayloads(String input)
      throws NewValueNotParsableException {
    String name = "";
    String payload = "";

    input = filterVoicePayloadPhonetics(handlePossibleSplitsUsingAND(input));

    List<String> wordList = new ArrayList<>(Arrays.asList(input.split(" ")));
    wordList.replaceAll(String::trim); // trim all entries
    wordList.removeAll(Collections.singleton(null)); // remove all nulls from all entries.
    wordList.removeAll(Collections.singleton("")); // remove all blanks from all entries.

    if ("off".equalsIgnoreCase(wordList.get(0))) {
      payload = "0";
      name = buildStringFromSubArray(wordList, 1, wordList.size() - 1);
    } else if ("off".equalsIgnoreCase(wordList.get(wordList.size() - 1))) {
      payload = "0";
      name = buildStringFromSubArray(wordList, 0, wordList.size() - 2);
    } else {
      // probably an 'on' condition. check to see if it's an attempt to assign an integer to a
      // device.
      int locationOfInteger = findAnInteger(wordList);
      if (locationOfInteger > -1) {
        // this is an 'on' condition where the newValue is a number.
        payload = String.valueOf(makeNumberReasonable(wordList.get(locationOfInteger)));
        name = getNameFromIntegerDeviceAndUpdateRequestContent(wordList);
      } else {
        // it's probably just a standard on-off device and the newValue is 'on'. let's make sure.
        if ("on".equalsIgnoreCase(wordList.get(0))) {
          payload = "1";
          name = buildStringFromSubArray(wordList, 1, wordList.size() - 1);
        } else if ("on".equalsIgnoreCase(wordList.get(wordList.size() - 1))) {
          payload = "1";
          name = buildStringFromSubArray(wordList, 0, wordList.size() - 2);
        }
      }
    }

    name = name.trim();
    name = name.replace(" ", "_");
    payload = payload.trim();

    /**
     * If the payload was missing, it could happen that the deviceName is not able to be discerned.
     * The entire contents of the wordList here IS the command. Send it back, and later a payload
     * will be added to it (this is the likely scneario of an "and" command. e.g.: "turn off the
     * kitchen light and the foyer light")
     */
    if (StringUtils.isEmpty(name) && StringUtils.isEmpty(payload)) {
      name = String.join("_", wordList);
    }

    return new FreeformPayloadFiltered(name.trim(), payload.trim());
  }

  private static String getNameFromIntegerDeviceAndUpdateRequestContent(List input) {
    List output = new ArrayList();
    for (int index = 0; index < input.size(); index++) {
      String value = ((String) (input.get(index)));
      if (!value.equalsIgnoreCase("on") && !(value).equalsIgnoreCase("to") && !isNumeric(value)) {
        output.add(value);
      }
    }
    return String.join(" ", output);
  }
}
