package piveraproject.utilities;

public class Constants {
  public static final String LOG_DATE_FORMAT = "yyyy-MM-dd hh:mm:ss a";
  public static final int SECONDS_DELAY_BEFORE_DEVICE_STATE_READ = 1;
  public static final int SECONDS_DELAY_BETWEEN_PROCESSING_SUBSEQUENT_VOICE_REQUESTS = 0;
}
