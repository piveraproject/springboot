package piveraproject.utilities;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import piveraproject.adapters.GenericItemJSONAdapter;
import piveraproject.device_components.SelectableState;
import piveraproject.device_models.GenericItem;
import piveraproject.dto.TwilioConfig;
import piveraproject.dto.User;
import piveraproject.exceptions.NewValueNotParsableException;

public class Utilities {

  public static final String ACTIVITY_FILE = "/logs/history_activity.txt";
  public static final String PHONETIC_FAILED_REQUEST_FILE =
      "/logs/history_requests_phonetic_failures.txt";
  public static final String OUT_TO_VERA_COMMAND_FILE = "/logs/history_commands_to_vera.txt";
  public static final String JAVA_ERROR_FILE = "/logs/java_errors.txt";

  public static void logInfo(String input) {
    System.out.println(input);
    writeToFile(ACTIVITY_FILE, input);
  }

  public static void logPossiblePhoneticsFailure(String input) {
    writeToFile(PHONETIC_FAILED_REQUEST_FILE, input);
  }

  public static void logOutputToVera(String input) {
    writeToFile(OUT_TO_VERA_COMMAND_FILE, input);
  }

  public static void logJavaError(String input) {
    System.out.println(input);
    writeToFile(JAVA_ERROR_FILE, input);
  }

  public static void writeToFile(String fileName, String data) {
    try {
      SimpleDateFormat simpleDateFormat =
          new SimpleDateFormat(Constants.LOG_DATE_FORMAT, new Locale("en", "US"));

      // Set true for append mode
      BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true));
      writer.newLine();
      writer.write(simpleDateFormat.format(new Date()) + "   " + data);
      writer.close();
    } catch (IOException e) {
      // not much we can do here.
      System.out.println("FileIO exception:" + e.getCause() + " " + e.getMessage());
    }
  }

  public static boolean isNumeric(String strNum) {
    Pattern pattern = Pattern.compile("^[0-9]*[1-9][0-9]*$");
    if (strNum == null) {
      return false;
    }
    return pattern.matcher(strNum).matches();
  }

  public static String buildStringFromSubArray(List array, int index1, int index2) {
    List output = new ArrayList();
    for (int x = index1; x <= index2; x++) {
      output.add(array.get(x));
    }
    return String.join(" ", output);
  }

  public static int findAnInteger(List<String> sentence) {
    for (int index = 0; index < sentence.size(); index++) {
      if (isNumeric(sentence.get(index))) {
        return index;
      }
    }
    return -1;
  }

  public static Map<String, GenericItem> readDevicesFromFile() {
    Map<String, GenericItem> devices = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

    String json = "";
    try {
      json = new String(Files.readAllBytes(Paths.get("configs/devices.txt")));
    } catch (IOException e) {
      Utilities.logJavaError(
          "Exception found while reading Devices File: " + e.getCause() + " " + e.getMessage());
    }

    try {
      GenericItem[] devicesFromFile =
          new GsonBuilder()
              .registerTypeAdapter(GenericItem.class, new GenericItemJSONAdapter())
              .excludeFieldsWithoutExposeAnnotation()
              .create()
              .fromJson(json, GenericItem[].class);

      // Build a map that is easy to index/read later.
      for (GenericItem deviceFromFile : devicesFromFile) {
        devices.put(deviceFromFile.getName().getKey(), deviceFromFile);
      }
    } catch (Exception e) {
      Utilities.logJavaError(
          "Exception found while parsing Devices File: " + e.getCause() + " " + e.getMessage());
    }

    return devices;
  }

  public static ArrayList<User> readPermissionsFromFile() {
    ArrayList<User> permissionList = new ArrayList<>();
    String json = "";
    try {
      json = new String(Files.readAllBytes(Paths.get("configs/permissions.txt")));
    } catch (IOException e) {
      Utilities.logJavaError(
          "Exception found while reading Permissions File: " + e.getCause() + " " + e.getMessage());
    }

    try {
      permissionList.addAll(Arrays.asList(new Gson().fromJson(json, User[].class)));
    } catch (Exception e) {
      Utilities.logJavaError(
          "Exception found while parsing Permissions File: " + e.getCause() + " " + e.getMessage());
    }
    return permissionList;
  }

  public static String computeButtonValueBasedOnStateAndActualValue(
      SelectableState state, int deviceCurrentValue) {
    if (((state.getOutboundControllerValue() == 0)
            && (state.getOutboundControllerValue() == deviceCurrentValue))
        || ((deviceCurrentValue > 0) && (state.getOutboundControllerValue() == 1))) {
      return "1";
    }
    return "0";
  }

  public static String readAuthTokenFromTwilioConfigFromFile() {
    String json = "";
    try {
      json = new String(Files.readAllBytes(Paths.get("configs/twilio_config.txt")));
    } catch (IOException e) {
      Utilities.logJavaError(
          "Exception found while reading Twilio Config File: "
              + e.getCause()
              + " "
              + e.getMessage());
    }
    TwilioConfig config = null;
    try {
      config = new Gson().fromJson(json, TwilioConfig.class);
      if (config == null || StringUtils.isEmpty(config.getAuthToken())) {
        Utilities.logJavaError(
            "Exception found while parsing Twilio Config File: config missing auth token");
      }
    } catch (Exception e) {
      Utilities.logJavaError(
          "Exception found while parsing Twilio Config File: "
              + e.getCause()
              + " "
              + e.getMessage());
    }
    return config.getAuthToken();
  }

  public static int makeNumberReasonable(String input) throws NewValueNotParsableException {
    int newValue;
    try {
      if (input.contains(".")) {
        input = input.substring(0, input.indexOf("."));
      }
      newValue = Integer.parseInt(input);
    } catch (Exception e) {
      throw new NewValueNotParsableException("newValue");
    }
    newValue = Math.abs(newValue);

    // There's a unique situation when the GoogleAssistant will interpret an integer
    // request like "turn on the fan to 50" as "turn on the fan 250". For this situation,
    // ignore everything to the left of the tens digit.
    if (newValue > 100) {
      newValue = newValue % 100;
    } else if (newValue < 0) {
      newValue = 0;
    }
    return newValue;
  }

  public static int change1to100(int input) {
    /**
     * For devices that can take an integer, a newValue of "1" needs to be changed to "100". This is
     * because some input devices like the Google Assistant will be used by people saying "turn on
     * the [device]", which will be interpreted as having a newValue of "1", which will be very dim.
     * The user likely means "turn it all the way up" in these cases.
     */
    if (input == 1) {
      return 100;
    }
    return input;
  }

  public static boolean doesPersonHavePermission(User person, String permission) {
    if (person == null
        || CollectionUtils.isEmpty(person.getPermissions())
        || StringUtils.isEmpty(permission)) {
      return false;
    }

    List<String> result =
        person.getPermissions().stream().map(String::toUpperCase).collect(Collectors.toList());
    return result.contains(permission.toUpperCase());
  }
}
