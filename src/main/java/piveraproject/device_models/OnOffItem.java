package piveraproject.device_models;

import static piveraproject.utilities.Constants.SECONDS_DELAY_BEFORE_DEVICE_STATE_READ;

import com.google.gson.annotations.Expose;
import java.util.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.client.RestTemplate;
import piveraproject.device_components.SelectableState;
import piveraproject.exceptions.NewValueNotAllowedException;
import piveraproject.services.VeraConnectionService;
import piveraproject.utilities.Utilities;

public class OnOffItem extends GenericItem {
  @Expose private ArrayList<SelectableState> eligibleStates = new ArrayList();

  public OnOffItem() {
    this.itemType = this.getClass().getSimpleName();
  }

  public SelectableState getIntendedNewStateBasedOnTrigger(String trigger) {
    return eligibleStates.stream()
        .filter(e -> e.getTrigger().equalsIgnoreCase(trigger))
        .findAny()
        .orElse(null);
  }

  public OnOffItem setOnSelectableState(SelectableState onSelectableState) {
    eligibleStates.add(onSelectableState);
    return this;
  }

  public OnOffItem setOffSelectableState(SelectableState offSelectableState) {
    eligibleStates.add(offSelectableState);
    return this;
  }

  public ArrayList<SelectableState> getEligibleStates() {
    return eligibleStates;
  }

  @Override
  public int postToVera(String input, VeraConnectionService veraConnectionService)
      throws NewValueNotAllowedException {
    SelectableState newState = getIntendedNewStateBasedOnTrigger(input);

    if (newState == null) {
      throw new NewValueNotAllowedException();
    }
    String url =
        veraConnectionService.VERA_HTTP_DEVICE_UPDATE_HEADER
            + newState.getAddress()
            + veraConnectionService.VERA_DEVICE_UPDATE_SERVICE_ID
            + veraConnectionService.VERA_ON_OFF_SET_COMMAND
            + newState.getOutboundControllerValue();

    Utilities.logOutputToVera(url);
    new RestTemplate().getForEntity(url, String.class);
    return SECONDS_DELAY_BEFORE_DEVICE_STATE_READ;
  }

  @Override
  public GenericItem getAndStoreCurrentValue(VeraConnectionService veraConnectionService) {
    // Get the current state of the device and store it.
    int tempAddress = -1;
    int deviceCurrentValue = 0;
    for (SelectableState state : eligibleStates) {
      if (tempAddress != state.getAddress()) {
        tempAddress = state.getAddress();
        String url =
            veraConnectionService.VERA_HTTP_GETTER_HEADER
                + state.getAddress()
                + veraConnectionService.VERA_DEVICE_UPDATE_SERVICE_ID
                + veraConnectionService.VERA_ON_OFF_GET_COMMAND;
        Utilities.logOutputToVera(url);
        String result = new RestTemplate().getForObject(url, String.class);
        if (StringUtils.isNumeric(result)) {
          deviceCurrentValue = Integer.parseInt(result);
        }
      }
      state.setCurrentValue(
          Utilities.computeButtonValueBasedOnStateAndActualValue(state, deviceCurrentValue));
    }
    return this;
  }
}
