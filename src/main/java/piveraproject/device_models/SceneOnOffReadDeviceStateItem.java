package piveraproject.device_models;

import com.google.gson.annotations.Expose;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.client.RestTemplate;
import piveraproject.device_components.SelectableState;
import piveraproject.services.VeraConnectionService;
import piveraproject.utilities.Utilities;

public class SceneOnOffReadDeviceStateItem extends SceneOnOffItem {
  @Expose private int actualDeviceAddress;

  public int getActualDeviceAddress() {
    return actualDeviceAddress;
  }

  public SceneOnOffReadDeviceStateItem setActualDeviceAddress(int actualDeviceAddress) {
    this.actualDeviceAddress = actualDeviceAddress;
    return this;
  }

  @Override
  public GenericItem getAndStoreCurrentValue(VeraConnectionService veraConnectionService) {
    String url =
        veraConnectionService.VERA_HTTP_GETTER_HEADER
            + actualDeviceAddress
            + veraConnectionService.VERA_DEVICE_UPDATE_SERVICE_ID
            + veraConnectionService.VERA_ON_OFF_GET_COMMAND;
    Utilities.logOutputToVera(url);
    String result = new RestTemplate().getForObject(url, String.class);
    if (StringUtils.isNumeric(result)) {
      int deviceCurrentValue = Integer.parseInt(result);
      for (SelectableState state : super.getEligibleStates()) {
        state.setCurrentValue(
            Utilities.computeButtonValueBasedOnStateAndActualValue(state, deviceCurrentValue));
      }
    }
    return this;
  }
}
