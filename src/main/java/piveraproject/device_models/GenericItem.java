package piveraproject.device_models;

import static piveraproject.utilities.Constants.SECONDS_DELAY_BEFORE_DEVICE_STATE_READ;

import com.google.gson.annotations.Expose;
import piveraproject.device_components.Name;

public abstract class GenericItem implements Actionable {

  @Expose private Name name;
  @Expose private boolean secure = false;
  @Expose private boolean requireConfirmation = false;

  @Expose(serialize = false) // don't allow this as an outgoing value.
  private String securityCategory;

  @Expose protected String itemType;

  public String getItemType() {
    return itemType;
  }

  public int getDelayBeforeDeviceStateRead() {
    return SECONDS_DELAY_BEFORE_DEVICE_STATE_READ;
  }

  public String getSecurityCategory() {
    return securityCategory;
  }

  public Name getName() {
    return name;
  }

  public boolean isSecure() {
    return secure;
  }

  public <T extends GenericItem> T setSecure(boolean secure) {
    this.secure = secure;
    return (T) this;
  }

  public <T extends GenericItem> T setName(Name name) {
    this.name = name;
    return (T) this;
  }
}
