package piveraproject.device_models;

import static piveraproject.utilities.Constants.SECONDS_DELAY_BEFORE_DEVICE_STATE_READ;

import org.springframework.web.client.RestTemplate;
import piveraproject.device_components.SelectableState;
import piveraproject.exceptions.NewValueNotAllowedException;
import piveraproject.services.VeraConnectionService;
import piveraproject.utilities.Utilities;

public class SceneOnOffItem extends OnOffItem {

  @Override
  public int postToVera(String input, VeraConnectionService veraConnectionService)
      throws NewValueNotAllowedException {
    SelectableState newState = getIntendedNewStateBasedOnTrigger(input);
    if (newState == null) {
      throw new NewValueNotAllowedException();
    }
    String url =
        veraConnectionService.VERA_HTTP_SCENE_RUN_UPDATE_HEADER
            + veraConnectionService.VERA_RUN_SCENE_SERVICE_ID
            + veraConnectionService.VERA_RUN_SCENE_COMMAND
            + newState.getAddress();
    Utilities.logOutputToVera(url);
    new RestTemplate().getForEntity(url, String.class);
    return SECONDS_DELAY_BEFORE_DEVICE_STATE_READ;
  }

  @Override
  public GenericItem getAndStoreCurrentValue(VeraConnectionService veraConnectionService) {
    // no-op, since Scenes don't have a "current state".
    return this;
  }
}
