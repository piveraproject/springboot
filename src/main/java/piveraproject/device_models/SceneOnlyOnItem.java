package piveraproject.device_models;

import static piveraproject.utilities.Constants.SECONDS_DELAY_BEFORE_DEVICE_STATE_READ;

import com.google.gson.annotations.Expose;
import org.springframework.web.client.RestTemplate;
import piveraproject.services.VeraConnectionService;
import piveraproject.utilities.Utilities;

public class SceneOnlyOnItem extends GenericItem {
  @Expose(serialize = false) // don't allow this as an outgoing value.
  private int address;

  @Expose private String buttonText;

  public SceneOnlyOnItem() {
    this.itemType = this.getClass().getSimpleName();
  }

  public SceneOnlyOnItem setButtonText(String buttonText) {
    this.buttonText = buttonText;
    return this;
  }

  public SceneOnlyOnItem setAddress(int address) {
    this.address = address;
    return this;
  }

  @Override
  public int postToVera(String input, VeraConnectionService veraConnectionService) {
    String url =
        veraConnectionService.VERA_HTTP_SCENE_RUN_UPDATE_HEADER
            + veraConnectionService.VERA_RUN_SCENE_SERVICE_ID
            + veraConnectionService.VERA_RUN_SCENE_COMMAND
            + address;
    Utilities.logOutputToVera(url);
    new RestTemplate().getForEntity(url, String.class);
    return SECONDS_DELAY_BEFORE_DEVICE_STATE_READ;
  }

  @Override
  public GenericItem getAndStoreCurrentValue(VeraConnectionService veraConnectionService) {
    // no-op, since Scenes don't have a "current state".
    return this;
  }
}
