package piveraproject.device_models;

import piveraproject.exceptions.NewValueNotAllowedException;
import piveraproject.exceptions.NewValueNotParsableException;
import piveraproject.services.VeraConnectionService;

public interface Actionable {
  int postToVera(String input, VeraConnectionService veraConnectionService)
      throws NewValueNotParsableException, NewValueNotAllowedException;

  GenericItem getAndStoreCurrentValue(VeraConnectionService veraConnectionService);
}
