package piveraproject.device_models;

import static piveraproject.utilities.Constants.SECONDS_DELAY_BEFORE_DEVICE_STATE_READ;

import com.google.gson.annotations.Expose;
import org.springframework.web.client.RestTemplate;
import piveraproject.exceptions.NewValueNotParsableException;
import piveraproject.services.VeraConnectionService;
import piveraproject.utilities.Utilities;

public class IntegerItem extends GenericItem {

  public IntegerItem() {
    this.itemType = this.getClass().getSimpleName();
  }

  @Expose(serialize = false) // don't allow this as an outgoing value.
  private int address;

  @Expose private String currentValue;

  public String getCurrentValue() {
    return currentValue;
  }

  public void setCurrentValue(String currentValue) {
    this.currentValue = currentValue;
  }

  public int getAddress() {
    return address;
  }

  public IntegerItem setAddress(int address) {
    this.address = address;
    return this;
  }

  @Override
  public int postToVera(String input, VeraConnectionService veraConnectionService)
      throws NewValueNotParsableException {
    int newValue = Utilities.makeNumberReasonable(input);
    newValue = Utilities.change1to100(newValue);
    String url =
        veraConnectionService.VERA_HTTP_DEVICE_UPDATE_HEADER
            + address
            + veraConnectionService.VERA_DEVICE_UPDATE_SERVICE_ID
            + veraConnectionService.VERA_INT_ON_OFF_SET_COMMAND
            + newValue;
    Utilities.logOutputToVera(url);
    new RestTemplate().getForEntity(url, String.class);
    return SECONDS_DELAY_BEFORE_DEVICE_STATE_READ;
  }

  @Override
  public GenericItem getAndStoreCurrentValue(VeraConnectionService veraConnectionService) {
    // Get the current state of the device and store it.

    final RestTemplate restTemplate = new RestTemplate();
    String url =
        veraConnectionService.VERA_HTTP_GETTER_HEADER
            + address
            + veraConnectionService.VERA_DEVICE_UPDATE_SERVICE_ID
            + veraConnectionService.VERA_INT_ON_OFF_GET_COMMAND;
    Utilities.logOutputToVera(url);
    currentValue = restTemplate.getForObject(url, String.class);
    return this;
  }
}
