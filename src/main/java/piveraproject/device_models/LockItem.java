package piveraproject.device_models;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.client.RestTemplate;
import piveraproject.device_components.SelectableState;
import piveraproject.exceptions.NewValueNotAllowedException;
import piveraproject.services.VeraConnectionService;
import piveraproject.utilities.Utilities;

public class LockItem extends SceneOnOffReadDeviceStateItem {
  public static final int lockDelayBeforeRead = 8;

  @Override
  public int getDelayBeforeDeviceStateRead() {
    return lockDelayBeforeRead;
  }

  @Override
  public int postToVera(String input, VeraConnectionService veraConnectionService)
      throws NewValueNotAllowedException {
    SelectableState newState = getIntendedNewStateBasedOnTrigger(input);
    if (newState == null) {
      throw new NewValueNotAllowedException();
    }
    String url =
        veraConnectionService.VERA_HTTP_SCENE_RUN_UPDATE_HEADER
            + veraConnectionService.VERA_RUN_SCENE_SERVICE_ID
            + veraConnectionService.VERA_RUN_SCENE_COMMAND
            + newState.getAddress();
    Utilities.logOutputToVera(url);
    new RestTemplate().getForEntity(url, String.class);
    return lockDelayBeforeRead;
  }

  @Override
  public GenericItem getAndStoreCurrentValue(VeraConnectionService veraConnectionService) {
    final RestTemplate restTemplate = new RestTemplate();
    String url =
        veraConnectionService.VERA_HTTP_GETTER_HEADER
            + super.getActualDeviceAddress()
            + veraConnectionService.VERA_LOCK_SERVICE_ID
            + veraConnectionService.VERA_LOCK_GET_COMMAND;
    Utilities.logOutputToVera(url);
    String result = new RestTemplate().getForObject(url, String.class);
    if (StringUtils.isNumeric(result)) {
      int deviceCurrentValue = Integer.parseInt(result);
      for (SelectableState state : super.getEligibleStates()) {
        state.setCurrentValue(
            Utilities.computeButtonValueBasedOnStateAndActualValue(state, deviceCurrentValue));
      }
    }
    return this;
  }
}
