package piveraproject.twilio;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;

public class TwilioUtils {
  public static final String TWILIO_SIGNATURE_HEADER_FIELD = "X-Twilio-Signature";

  public static String getRequestUrlAndQueryString(HttpServletRequest request) {
    String queryString = request.getQueryString();
    String requestUrl = request.getRequestURL().toString().replace("http", "https");
    if (queryString != null && queryString != "") {
      return requestUrl + "?" + queryString;
    }
    return requestUrl;
  }

  public static String getTwilioSignatureFromHeader(HttpServletRequest request) {
    return request.getHeader(TWILIO_SIGNATURE_HEADER_FIELD);
  }

  public static Map<String, String> extractPostParams(HttpServletRequest request) {
    String queryString = request.getQueryString();
    Map<String, String[]> requestParams = request.getParameterMap();
    List<String> queryStringKeys = getQueryStringKeys(queryString);

    return requestParams.entrySet().stream()
        .filter(e -> !queryStringKeys.contains(e.getKey()))
        .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()[0]));
  }

  public static List<String> getQueryStringKeys(String queryString) {
    if (queryString == null || queryString.length() == 0) {
      return Collections.emptyList();
    } else {
      return Arrays.stream(queryString.split("&"))
          .map(pair -> pair.split("=")[0])
          .collect(Collectors.toList());
    }
  }
}
