package piveraproject.dto;

import java.util.ArrayList;
import org.springframework.util.StringUtils;

public class User {
  private String name;
  private String userID;
  private String password;
  private ArrayList<String> permissions;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ArrayList<String> getPermissions() {
    return permissions;
  }

  public User setPermissions(ArrayList<String> permissions) {
    this.permissions = permissions;
    return this;
  }

  public String getUserID() {
    return userID;
  }

  public User setUserID(String userID) {
    this.userID = userID;
    return this;
  }

  public String getPassword() {
    return password;
  }

  public User setPassword(String password) {
    this.password = password;
    return this;
  }

  public boolean isPasswordCorrect(String input) {
    return !StringUtils.isEmpty(input) && input.equalsIgnoreCase(password);
  }
}
