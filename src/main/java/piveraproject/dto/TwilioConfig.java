package piveraproject.dto;

public class TwilioConfig {
  private String authToken = "";

  public String getAuthToken() {
    return authToken;
  }

  public TwilioConfig setAuthToken(String authToken) {
    this.authToken = authToken;
    return this;
  }
}
