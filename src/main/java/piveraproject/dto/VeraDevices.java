package piveraproject.dto;

import java.util.Map;
import java.util.TreeMap;
import piveraproject.device_models.GenericItem;
import piveraproject.exceptions.DeviceNotFoundException;

public class VeraDevices {

  Map<String, GenericItem> devices = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

  public Map<String, GenericItem> getDevices() {
    return devices;
  }

  public VeraDevices setDevices(Map<String, GenericItem> devices) {
    this.devices = devices;
    return this;
  }

  public GenericItem getDeviceFromName(String value) throws DeviceNotFoundException {
    GenericItem device = devices.get(value);
    if (device == null) {
      throw new DeviceNotFoundException();
    }
    return device;
  }
}
