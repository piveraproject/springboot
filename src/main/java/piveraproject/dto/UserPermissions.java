package piveraproject.dto;

import java.util.ArrayList;

public class UserPermissions {
  private ArrayList<User> permissionList = new ArrayList<>();

  public UserPermissions setPermissionList(ArrayList<User> permissionList) {
    this.permissionList = permissionList;
    return this;
  }

  public ArrayList<User> getPermissionList() {
    return permissionList;
  }
}
