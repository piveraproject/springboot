package piveraproject.dto;

public class FreeformJSON {
  private String userID;
  private String password;
  private String message;

  public String getUserID() {
    return userID;
  }

  public FreeformJSON setUserID(String userID) {
    this.userID = userID;
    return this;
  }

  public String getPassword() {
    return password;
  }

  public FreeformJSON setPassword(String password) {
    this.password = password;
    return this;
  }

  public String getMessage() {
    return message;
  }

  public FreeformJSON setMessage(String message) {
    this.message = message;
    return this;
  }
}
