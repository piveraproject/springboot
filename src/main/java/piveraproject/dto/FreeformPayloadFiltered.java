package piveraproject.dto;

public class FreeformPayloadFiltered {
  public String name;
  public String payload;

  public FreeformPayloadFiltered(String name, String payload) {
    this.name = name;
    this.payload = payload;
  }
}
