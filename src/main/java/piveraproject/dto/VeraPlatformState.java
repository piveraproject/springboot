package piveraproject.dto;

import piveraproject.exceptions.StateNotFoundException;

public class VeraPlatformState {
  public String id;
  public String service;
  public String variable;
  public String value;

  public static VeraPlatformState findStateByVariableName(
      VeraPlatformState[] veraPlatformStates, String key) throws StateNotFoundException {
    for (VeraPlatformState veraPlatformState : veraPlatformStates) {
      if (veraPlatformState.variable.equalsIgnoreCase(key)) {
        return veraPlatformState;
      }
    }
    throw new StateNotFoundException();
  }
}
