package piveraproject.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static piveraproject.utilities.Utilities.isNumeric;
import static piveraproject.utilities.Utilities.makeNumberReasonable;
import static piveraproject.utilities.WordFilter.filterVoicePayloadPhonetics;
import static piveraproject.utilities.WordFilter.handlePossibleSplitsUsingAND;
import static piveraproject.utilities.WordManipulator.convertVoiceInputToDevicesAndPayloads;
import static piveraproject.utilities.WordManipulator.splitByWordANDandAlsoShareNewValue;

import java.util.ArrayList;
import org.junit.Test;
import piveraproject.dto.FreeformPayloadFiltered;
import piveraproject.exceptions.NewValueNotParsableException;

public class VeraDevicesServiceTest {

  @Test
  public void testIsNumeric() {
    assertTrue(isNumeric("5"));
    assertTrue(isNumeric("55"));
    assertTrue(isNumeric("5555555"));
    assertTrue(isNumeric("055"));

    assertFalse(isNumeric("5.05"));
    assertFalse(isNumeric("-5.05"));
    assertFalse(isNumeric("-200"));
    assertFalse(isNumeric("-2"));
    assertFalse(isNumeric("-.2"));
    assertFalse(isNumeric("-0.2"));
    assertFalse(isNumeric(null));
    assertFalse(isNumeric("abc"));
  }

  @Test
  public void makeNumberReasonableTest() throws NewValueNotParsableException {
    assertEquals(makeNumberReasonable("1"), 1);

    assertEquals(makeNumberReasonable("0"), 0);
    assertEquals(makeNumberReasonable("0.0"), 0);
    assertEquals(makeNumberReasonable("000000"), 0);
    assertEquals(makeNumberReasonable("000000.0"), 0);
    assertEquals(makeNumberReasonable("000000.000"), 0);
    assertEquals(makeNumberReasonable("-0"), 0);
    assertEquals(makeNumberReasonable("-0.0"), 0);
    assertEquals(makeNumberReasonable("-000000"), 0);
    assertEquals(makeNumberReasonable("-000000.000"), 0);

    assertEquals(makeNumberReasonable("50"), 50);
    assertEquals(makeNumberReasonable("-50"), 50);
    assertEquals(makeNumberReasonable("50.0"), 50);
    assertEquals(makeNumberReasonable("-50.0"), 50);

    assertEquals(makeNumberReasonable("150"), 50);
    assertEquals(makeNumberReasonable("250"), 50);
    assertEquals(makeNumberReasonable("1050"), 50);

    assertEquals(makeNumberReasonable("-150"), 50);
    assertEquals(makeNumberReasonable("-250"), 50);
    assertEquals(makeNumberReasonable("-1050"), 50);
    assertEquals(makeNumberReasonable("-150.0"), 50);
    assertEquals(makeNumberReasonable("-250.0"), 50);
    assertEquals(makeNumberReasonable("-1050.0"), 50);
  }

  @Test
  public void Test1() {
    assertTrue(
        executeTest(
            "turn off all the kitchen lights",
            new FreeformPayloadFiltered("kitchen_light", "0")));
  }


    @Test
    public void Test2() {
        assertTrue(
                executeTest(
                        "turn on all the kitchen lights and the bedside lamps",
                        new ArrayList<FreeformPayloadFiltered>() {
                            {
                                add(new FreeformPayloadFiltered("kitchen_light", "1"));
                                add(new FreeformPayloadFiltered("bedside_light", "1"));
                            }
                        }));
    }

  public boolean executeTest(String input, FreeformPayloadFiltered expected) {

    FreeformPayloadFiltered filtered = null;
    try {
      filtered =
          convertVoiceInputToDevicesAndPayloads(
              filterVoicePayloadPhonetics(handlePossibleSplitsUsingAND(input)));
    } catch (NewValueNotParsableException e) {
      fail();
    }

    boolean wasSuccessful =
        expected.name.equalsIgnoreCase(filtered.name)
            && expected.payload.equalsIgnoreCase(filtered.payload);

    if (!wasSuccessful) {
      System.out.println("====================================");
      System.out.println("input: " + input);
      System.out.println("expected " + expected.name + "," + expected.payload);
      System.out.println("output " + filtered.name + "," + filtered.payload);
    }
    return wasSuccessful;
  }

  public boolean executeTest(String inputRaw, ArrayList<FreeformPayloadFiltered> expectedArray) {

    ArrayList<FreeformPayloadFiltered> outputs = null;
    try {
      outputs = splitByWordANDandAlsoShareNewValue(inputRaw);
    } catch (NewValueNotParsableException e) {
      fail();
    }

    boolean wasOverallSuccessful = true;
    for (int x = 0; x < expectedArray.size(); x++) {
      FreeformPayloadFiltered result = outputs.get(x);
      FreeformPayloadFiltered expected = expectedArray.get(x);
      if (!result.name.equalsIgnoreCase(expected.name)
          || !result.payload.equalsIgnoreCase(expected.payload)) {
        wasOverallSuccessful = false;
      }
    }

    if (!wasOverallSuccessful) {
      StringBuilder expectedOutput = new StringBuilder();
      for (FreeformPayloadFiltered expected : expectedArray) {
        expectedOutput.append("command: ");
        expectedOutput.append(expected.name);
        expectedOutput.append(" ");
        expectedOutput.append("payload: ");
        expectedOutput.append(expected.payload);
        expectedOutput.append(" ");
      }

      StringBuilder actualOutput = new StringBuilder();
      for (FreeformPayloadFiltered output : outputs) {
        actualOutput.append("command: ");
        actualOutput.append(output.name);
        actualOutput.append(" ");
        actualOutput.append("payload: ");
        actualOutput.append(output.payload);
        actualOutput.append(" ");
      }

      System.out.println("====================================");
      System.out.println("input: " + inputRaw);
      System.out.println("expected " + expectedOutput.toString());
      System.out.println("output " + actualOutput.toString());
    }
    return wasOverallSuccessful;
  }
}
