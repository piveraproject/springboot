package piveraproject.utilities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import org.junit.Test;
import piveraproject.dto.FreeformPayloadFiltered;

public class WordManipulatorTest {
  @Test
  public void validatePayloadSharingLogic() {
    ArrayList<FreeformPayloadFiltered> outputs = new ArrayList();
    final String expectedPayload = "payload1";
    FreeformPayloadFiltered item1 = new FreeformPayloadFiltered("name1", expectedPayload);
    FreeformPayloadFiltered item2 = new FreeformPayloadFiltered("name2", "");
    FreeformPayloadFiltered item3 = new FreeformPayloadFiltered("name3", "");
    outputs.add(item1);
    outputs.add(item2);
    outputs.add(item3);
    outputs = WordManipulator.applyFirstFoundPayloadToAllBlankPayloads(outputs);
    assertTrue(outputs.size() == 3);
    for (FreeformPayloadFiltered output : outputs) {
      assertEquals(expectedPayload, output.payload);
    }
  }
}
